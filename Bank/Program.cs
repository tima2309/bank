﻿using System;
using System.Collections.Generic;

namespace Bank
{
     class Client
    {
        public string Name;
        public string Surname;
        public int Age;
        public int Balanse;

        public Client()
        {
            Name = "name";
            Surname = "surname";
            Age = 0;
            Balanse = 0;
        }
        public Client(string name, string surname, int age, int balanse)
        {
            Name = name;
            Surname = surname;
            Age = age;
            Balanse = balanse;
        }

        public void ChangeName(string name) => Name = name;
        public void ChangeSurname(string surname) => Surname = surname;
        public void ChangeAge(int age) => Age = age;
        public void ChangeBalanse(int balanse) => Balanse = balanse;

        public void ShowClient()
        {
            Console.WriteLine($"Имя: {Name} \n Фамилия: {Surname} \n Возраст: {Age} \n Баланс: {Balanse}");
        }
    }
}
