﻿using System;
using System.Collections.Generic;

namespace Bank
{
   class Mybank
    {
        public static string BankName = "Добробыт";
        public static string BankAdress = "Минск ул Городецкая д 25";

        public static List<Client> ClientList = new List<Client>();
        int count = Convert.ToInt32(Console.ReadLine());

        public static void Abb()
        {
            Console.Clear();
            Console.WriteLine("Имя клиента");
            string name = Console.ReadLine();
            Console.WriteLine("Фамилия клиента");
            string surname = Console.ReadLine();
            Console.WriteLine("Возраст");
            int age = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Баланс");
            int balanse = Convert.ToInt32(Console.ReadLine());

            Console.Clear();
            Console.WriteLine($"Добавлен клиент:{name} {surname}");
            Client client = new Client(name, surname, age, balanse);
            ClientList.Add(client);
        }

        public static void Remove()
        {
            Console.Clear();
            Console.WriteLine("Введите фамилию клиента которого необходимо удалить");
            string surname = Console.ReadLine();

            for (int i = 0; i < ClientList.Count; i++)
            {
                if (surname == ClientList[i].Surname)
                {
                    string clientname = ClientList[i].Name;
                    string clientsurname = ClientList[i].Surname;
                    ClientList.Remove(ClientList[i]);
                    Console.WriteLine($"{clientname} {clientsurname} удален");
                    Console.ReadLine();
                }
            }

        }
        public static void ChangeBalanse()
        {
            string surname = Console.ReadLine();
            for (int i = 0; i < ClientList.Count; i++)
            {
                if (surname == ClientList[i].Surname)
                {
                    Console.WriteLine($"Текущий баланс: {ClientList[i].Balanse} \n Введите баланс клиента:");
                    int balanse = Convert.ToInt32(Console.ReadLine());
                    ClientList[i].Balanse = balanse;
                }
            }
        }
        public static void ShowClient()
        {
            Console.Clear();
            Console.WriteLine("Введите фамилию чтобы получить информацию о клиенте");
            string surname = Console.ReadLine();
            for (int i = 0; i < ClientList.Count; i++)
            {
                if (surname == ClientList[i].Surname)
                {
                    Console.WriteLine($"Имя: {ClientList[i].Name} \n Фамилия: {ClientList[i].Surname} \n Возраст: {ClientList[i].Age} \n Баланс {ClientList[i].Balanse}");

                }
            }
            Console.ReadLine();
        }
        public static void ShowAllClients()
        {
            Console.Clear();
            if (ClientList.Count == 0)
            {
                Console.WriteLine($"Нет клиетов :(");
            }
            else
            {
                Console.WriteLine($"Список клиентов банка {ClientList.Count}\n================\n");
                ClientList.Sort((left, right) => left.Surname.CompareTo(right.Surname));
                foreach (Client sortedClientList in ClientList)
                {
                    Console.WriteLine($"Имя: {sortedClientList.Name}\n Фамилия: {sortedClientList.Surname}\n Возраст: {sortedClientList.Age}\n Баланс: {sortedClientList.Balanse}");
                }
            }
            Console.WriteLine("Чтобы продолжить нажьмите любую кнопку");
            Console.ReadLine();
        }



    }
    class Mainclass
    {
        public static void Main(string[] args)
        {
            bool cycle = true;
            while (cycle)
            {
                Console.Clear();
                Console.WriteLine($"{Mybank.BankName}\n {Mybank.BankAdress}");
                Console.WriteLine("=============\n\n1.Добавить клиента\n2.Удалить клиента\n3.Изменить имя или фамилию клиента\n4.Изменить баланс клиента\n5.Информация о клиенте");
                int num = Convert.ToInt32(Console.ReadLine());
                switch (num)
                {
                    case 1:
                        Mybank.Abb();
                        break;
                    case 2:
                        Mybank.Remove();
                        break;
                    case 3:
                        Mybank.ChangeBalanse();
                        break;
                    case 4:
                        Mybank.ShowClient();
                        break;
                    case 5:
                        Mybank.ShowAllClients();
                        break;
                    case 6:
                        cycle = false;
                        break;
                }
            }
        } 
    }
}
